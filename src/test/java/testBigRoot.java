import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class testBigRoot {
    @Test(expected = Exception.class)
    public void testDLowerThanZeroOrAIsZero() {
        Twothree th = new Twothree(0, 1, 1);
        Biggestroot br = new Biggestroot(th);
        br.getMaxRoot();
    }

    @Test
    public void testDEqualsZero() {
        Twothree th = new Twothree(1, 2, 1); //ura
        Biggestroot br = new Biggestroot(th);
        assertEquals(br.getMaxRoot(), -1, 10E-9);
    }

    @Test
    public void testDBiggerThanZero() {
        Twothree th = new Twothree(1, 2, -3);
        Biggestroot br = new Biggestroot(th);
        assertEquals(br.getMaxRoot(), 1, 10E-9);
    }

}
