public class Biggestroot {
        private Twothree th;

        public Biggestroot (Twothree th)
        {
            this.th = th;
        }

        public double getMaxRoot() {
            if (th.getRoots() == null) {
                throw new IllegalArgumentException();
            } else {
                return (th.getRoots()[0] > th.getRoots()[1]) ? th.getRoots()[0] : th.getRoots()[1];
            }
        }

}
